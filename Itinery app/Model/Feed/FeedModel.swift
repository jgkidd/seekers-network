//
//  FeedModel.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit


class FeedModel {
    let id: UUID
    var title: String
    var guideImage: UIImage?
    var creatorsAvatar: UIImage?
    var reviewCount: String?
    var numberOfLikes: String?
    var costOfGuide: String?
    var numberOfShares: String?
    var viewersAvatar: UIImage?
    var numberOfComments: String?
    
    
    init(title: String, guideImage: UIImage? = nil, creatorsAvatar: UIImage? = nil, reviewCount: String? = nil, numberOfLikes: String? = nil, costOfGuide: String? = nil, numberOfShares: String? = nil, viewersAvatar: UIImage? = nil, numberOfComments: String? = nil ) {
        id = UUID()
        self.title = title
        self.guideImage = guideImage
        self.creatorsAvatar = creatorsAvatar
        self.reviewCount = reviewCount
        self.numberOfLikes = numberOfLikes
        self.costOfGuide = costOfGuide
        self.numberOfShares = numberOfShares
        self.viewersAvatar = viewersAvatar
        self.numberOfComments = numberOfComments
        
    }
}

