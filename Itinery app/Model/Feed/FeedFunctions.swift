//
//  feedFunctions.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import Foundation

class FeedFunctions {
    static func createPost(feedModel: FeedModel) {
        FeedData.feedModels.append(feedModel)
    }
    
    static func readPost (completion: @escaping () -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
            if FeedData.feedModels.count == 0 {
                FeedData.feedModels.append(FeedModel(title: "Paris", guideImage: #imageLiteral(resourceName: "Paris-1"), creatorsAvatar: #imageLiteral(resourceName: "sh-avatar"), reviewCount: "12", numberOfLikes: "22", costOfGuide: "Free", numberOfShares: "6", viewersAvatar: #imageLiteral(resourceName: "sh-avatar"), numberOfComments: "6"))
                FeedData.feedModels.append(FeedModel(title: "Tahiti", guideImage: #imageLiteral(resourceName: "tahiti-1"), creatorsAvatar: #imageLiteral(resourceName: "jr-avatar"), reviewCount: "8", numberOfLikes: "53", costOfGuide: "Free", numberOfShares: "15", viewersAvatar: #imageLiteral(resourceName: "sh-avatar"), numberOfComments: "6"))
                FeedData.feedModels.append(FeedModel(title: "Istanbul", guideImage: #imageLiteral(resourceName: "Instanbul 3"), creatorsAvatar: #imageLiteral(resourceName: "jk-avatar"), reviewCount: "32", numberOfLikes: "75", costOfGuide: "Free", numberOfShares: "38", viewersAvatar: #imageLiteral(resourceName: "sh-avatar"), numberOfComments: "6"))
            }
            DispatchQueue.main.async {
                completion()
            }
        }
        
    }
    
    static func updatePost (feedModel: FeedModel) {
        
    }
    
    static func deletePost (feedModel: FeedModel) {
        
    }
    
}
