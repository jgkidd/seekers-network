//
//  LoginHandler.swift
//  Itinery app
//
//  Created by Josh on 19/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase


class LoginHandler {
    init() {
    }
    
    // Fetch Facebook data
    
    
    public func fbGraphRequest(accessToken: String, currentUserID: String, completionHandler: ()){
        
        let currentUserID = currentUserID
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, picture.type(large)"]).start { (connection, result, err) in
            if err != nil {
                print("failed to start graph request:", err ?? "")
                return
            }
            guard let userInfo = result as? [String: Any] else { return }

            
            let fbID = userInfo["id"] as! String
            let fbEmail = userInfo["email"] as! String
            let fbName = userInfo["name"] as! String
            if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                
                let pulledFacebookAvatarURL = imageURL
                
                let url = URL(string: imageURL)
                let data = NSData(contentsOf: url!)
                let image = UIImage(data: data! as Data)
                
                //self.selectedImageForAvatar = image
                //self.profileAvatarImageView.image = image
                
                currUser.avatarURL = pulledFacebookAvatarURL
                currUser.userID = fbID
                currUser.name = fbName
                currUser.email = fbEmail
                currUser.avatarImage = image
                currUser.currentUserID = currentUserID
                
                self.addAccountCreationDataToDatabase(displayName: fbName, email: fbEmail, avatarImageURL: pulledFacebookAvatarURL, facebookID: fbID, completionHandler: ())
                
            }
        }
    }
    
    public func addAccountCreationDataToDatabase(displayName: String, email: String, avatarImageURL: String, facebookID: String?, completionHandler: ()){
        
        
        ProgressHUD.showSuccess("Sucess")


        let ref = Database.database().reference()
        let usersReference = ref.child("users")
        let newUserReference = usersReference.child(currUser.currentUserID)
        // add data to database
        newUserReference.setValue(["DisplayName": displayName, "email": email, "avatarImageURL": avatarImageURL, "Facebook ID": facebookID])


        print("finished adding to database")
    }
    
    
}
