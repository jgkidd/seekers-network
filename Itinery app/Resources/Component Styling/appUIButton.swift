//
//  appUIButton.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class appUIButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //backgroundColor = Theme.brandPrimaryColour
        layer.cornerRadius = frame.height / 2
        setTitleColor(UIColor.white, for: .normal)
        
        
    }

}
