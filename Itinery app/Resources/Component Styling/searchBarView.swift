//
//  searchBarView.swift
//  Seekers Prototype 002
//
//  Created by Josh on 14/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class searchBarView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 0.25
        layer.shadowColor = UIColor.darkGray.cgColor
    }
}
