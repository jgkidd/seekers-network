//
//  PopupUIView.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class PopupUIView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layer.shadowOpacity = 0.15
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 10
        
        backgroundColor = Theme.cards
    }

}
