//
//  profileAvatar.swift
//  Itinery app
//
//  Created by Josh on 19/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class profileAvatar: UIButton {
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
            layer.cornerRadius = frame.height / 2
            clipsToBounds = true
            contentMode = .scaleAspectFill
            

        }
    }

