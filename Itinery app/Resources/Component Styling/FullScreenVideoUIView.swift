//
//  FullScreenVideoUIView.swift
//  Itinery app
//
//  Created by Josh on 16/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit
import SwiftVideoBackground

class FullScreenVideoUIView: UIView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
     try? VideoBackground.shared.play(view: self, videoName: "welcome-vid", videoType: "mp4", isMuted: true, darkness: 0.5, willLoopVideo: true, setAudioSessionAmbient: true)
        
        
    }
}
