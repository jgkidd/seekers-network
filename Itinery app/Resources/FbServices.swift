//
//  FbGraphRequest.swift
//  Itinery app
//
//  Created by Josh on 19/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import Foundation
import FBSDKCoreKit

class FbServices {
    
    static let sharedInstance = FbServices()
    
    init() {
    }
    
    func getData(fbID: String, fbEmail: String, fbName: String, fbAvatarURL: String) -> Void {
        
        var fbID = String()
        var fbEmail = String()
        var fbName = String()
        var fbAvatarURL = String()
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, picture.type(large)"]).start { (connection, result, err) in
            if err != nil {
                print("failed to start graph request:", err ?? "")
                return
            }
            guard let userInfo = result as? [String: Any] else { return }
            
            fbID = userInfo["id"] as! String
            fbEmail = userInfo["email"] as! String
            fbName = userInfo["name"] as! String
            fbAvatarURL = (((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String ?? nil)!
            
        }
     
        //return (fbID, fbEmail, fbName, fbAvatarURL)
    }
}
