//
//  Theme.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class Theme {
    
    static let mainFontName = "AbrilDisplay-ExtraBold"
    
    static let brandPrimaryColour = UIColor(named: "Brand")
    static let cards = UIColor(named: "Card")

    static let tint = UIColor(named: "Tint")
    static let textColour = UIColor(named: "textColour")
   
    
    
    
}
