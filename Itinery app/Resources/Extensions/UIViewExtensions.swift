//
//  UIViewExtensions.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

extension UIView {

    func addShadowAndRoundedCorners() {
        layer.shadowOpacity = 0.15
        layer.shadowOffset = CGSize.zero
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.cornerRadius = 10
    }
    

}
