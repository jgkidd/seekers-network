//
//  UIButtonExtension.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

extension UIButton {

    func createFloatingActionButton() {
        backgroundColor = Theme.brandPrimaryColour
        layer.cornerRadius = frame.height / 2
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 10)
    }
    
    func avatarImageUpdate() {
        if currUser.avatarImage != nil {
           setImage(currUser.avatarImage, for: .normal)
        }
    }

}
