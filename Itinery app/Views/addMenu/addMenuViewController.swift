//
//  addMenuViewController.swift
//  Seekers Prototype 002
//
//  Created by Josh on 14/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class addMenuViewController: UIViewController {
    
    @IBOutlet weak var closeMenuButton: UIButton!
    @IBOutlet weak var menuBKG: UIView!
    @IBOutlet weak var newReviewButton: UIButton!
    @IBOutlet weak var newStoryButton: newMenuButtons!
    @IBOutlet weak var newVideoButton: newMenuButtons!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuBKG.alpha = 0
        newReviewButton.transform = CGAffineTransform(translationX: 140, y: 0)
        newStoryButton.transform = CGAffineTransform(translationX: 140, y: 0)
        newStoryButton.alpha = 0.5
        newVideoButton.transform = CGAffineTransform(translationX: 140, y: 0)
        newVideoButton.alpha = 0.5
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.1, animations: {
            self.menuBKG.alpha = 0.8
            self.closeMenuButton.transform = CGAffineTransform(rotationAngle: 45 * (.pi / 180))
        }) {
            (true) in
            self.showMenuButtons()
            }
    }
    
    func showMenuButtons(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {
            self.newReviewButton.transform = CGAffineTransform(translationX: 0, y: 0)
        })
        UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {
            self.newStoryButton.transform = CGAffineTransform(translationX: 0, y: 0)
        })
        UIView.animate(withDuration: 0.5, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.allowUserInteraction, .curveEaseOut], animations: {
            self.newVideoButton.transform = CGAffineTransform(translationX: 0, y: 0)
        })
    }
    
    
    
    @IBAction func closeMenuButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.closeMenuButton.transform = CGAffineTransform(rotationAngle: 0 * (.pi / 180))
        }) {
            (true) in
            self.dismiss(animated: true)
        }
        
    }
}
