//
//  searchViewController.swift
//  Seekers Prototype 002
//
//  Created by Josh on 14/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class searchViewController: UIViewController {
    
    @IBOutlet weak var menuBKG: UIView!
    @IBOutlet weak var searchBarField: UITextField!
    @IBOutlet weak var searchResults: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBKG.alpha = 0
        UIView.animate(withDuration: 0.1, animations: {
            self.menuBKG.alpha = 0.8
        })
        searchBarField.becomeFirstResponder()
        
        // Dismiss keyboard on drag outside of keyboard
        searchResults.keyboardDismissMode = .onDrag
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    @IBAction func swipeDownToClose(_ sender: UIPanGestureRecognizer) {
        
        let vel = sender.velocity(in: view)

        if vel.y > 0 {
            searchBarField.resignFirstResponder()
            dismiss(animated: true)
        }
    }
    
    
    @IBAction func reinstateFirstResponder(_ sender: Any) {
        searchBarField.becomeFirstResponder()
    }
}


extension searchViewController: UITableViewDelegate, UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.delegate = self
        searchBarField.resignFirstResponder()
    }
}
