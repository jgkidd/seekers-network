//
//  SharePopupViewController.swift
//  Itinery app
//
//  Created by Josh on 09/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class SharePopupViewController: UIViewController {
    
    @IBOutlet var animateSharePopup: UIView!
    @IBOutlet weak var sharePopupBKG: UIView!
    @IBOutlet weak var sharePostPopup: UIView!
    
    
    //var sharePostPopupHeight: String
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sharePostPopupHeight = sharePostPopup.frame.height
        
        print(sharePostPopupHeight)

        sharePopupBKG.alpha = 0
        sharePostPopup.transform = CGAffineTransform(translationX: 0, y: sharePostPopupHeight)
        
        sharePostPopup.layer.cornerRadius = 10
        sharePostPopup.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3){
            self.sharePopupBKG.alpha = 100
            self.sharePostPopup.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
    
    
    
    @IBAction func dismissPopup(_ sender: Any) {
        UIView.animate(withDuration: 0.3){
            self.sharePopupBKG.alpha = 0
            self.sharePostPopup.transform = CGAffineTransform(translationX: 0, y: self.sharePostPopup.frame.height)
        }
        dismiss(animated: true)
    }
    

}
