//
//  profileViewController.swift
//  Itinery app
//
//  Created by Josh on 14/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit
import Hero
import FirebaseAuth

class profileViewController: UIViewController {
    
    
    @IBOutlet weak var logoutButton: appUIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileToFeed"  {
            let dest = segue.destination as! FeedViewController
            dest.hero.modalAnimationType = .selectBy(presenting: .slide(direction: .right), dismissing: .slide(direction: .left))
        }
    }
    
    
    @IBAction func logoutButton(_ sender: Any) {
        do {
            try Auth.auth().signOut()
        } catch let logoutError {
            print(logoutError)
        }
        
        let sendLoggedOutUserTo = UIStoryboard(name: "LoginDemo", bundle: nil)
        let loginInViewController = sendLoggedOutUserTo.instantiateViewController(withIdentifier: "LoginLandingViewController")
        self.present(loginInViewController, animated: true, completion: nil)
    }
    

}
