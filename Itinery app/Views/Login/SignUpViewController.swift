//
//  SignUpViewController.swift
//  Itinery app
//
//  Created by Josh on 16/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FBSDKLoginKit

class SignUpViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    
    // OUTLETS
    
    @IBOutlet weak var displayNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var profileAvatarImageView: UIImageView!
    @IBOutlet weak var createAccountButton: appUIButton!
    
    
    // VARIABLES
    
    var selectedImageForAvatar: UIImage?
    var currentUserID = String()
    var pulledFacebookAvatarURL = String()
    
    
    // OVERRIDES
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func signUpBackButton(_ sender: Any) {
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Profile avatar styling
        
        profileAvatarImageView.layer.cornerRadius = profileAvatarImageView.frame.height / 2
        profileAvatarImageView.clipsToBounds = true
        profileAvatarImageView.contentMode = .scaleAspectFill
        profileAvatarImageView.layer.borderWidth = 5
        
        
        // Add FB login button to the view
        
        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        loginButton.frame = CGRect(x: 37, y: 559, width: 300, height: 50)
        loginButton.delegate = self
        loginButton.readPermissions = ["email"]
        
        
        // Create a tap guesture reconizer linked to the "profileAvatarImageView"
        
        let tapProfileAvatarGestureReconizer = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.handleSelectProfileImageView))
        profileAvatarImageView.addGestureRecognizer(tapProfileAvatarGestureReconizer)
        profileAvatarImageView.isUserInteractionEnabled = true
        
        createAccountButton.isEnabled = false
        createAccountButton.alpha = 0.5
        handleTextField()
    }
    
    
    // FUNCTIONS

    
    // Update login button active state when user details have been entered
    func handleTextField() {
        displayNameTextField.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        emailTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        
    }
    @objc func textFieldDidChange() {
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                createAccountButton.isEnabled = false
                return
        }
        createAccountButton.alpha = 1
        createAccountButton.isEnabled = true
    }
    
    
    // FB functions
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did log out of Facebook")
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        loginWithFacebook()
    }
    
    func loginWithFacebook(){
        
        // Show progress
        ProgressHUD.show("Creating account", interaction: false)
        
        // Get and store access token
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else { return }
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        
        // Auth with Firebase and FB credentrials
        Auth.auth().signInAndRetrieveData(with: credentials, completion: { (user, error) in
            if error != nil {
                print("Something went wrong with Facebook user: ", error ?? "")
                return
            }
            print("User was sucessfully logged in: ", user ?? "")
            
            // Get Firebase UID and run FB Login
            self.currentUserID = user!.user.uid
            let LoginHandlerInstance = LoginHandler()
            LoginHandlerInstance.fbGraphRequest(accessToken: accessTokenString, currentUserID: self.currentUserID, completionHandler: self.gotoHomeScreen())
        })
    }
    
    // removed function from here see login for code
    
    // Listen for avatar tap
    
    @objc func handleSelectProfileImageView(){
        print("tapping")
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        present(pickerController, animated: true, completion: nil)
    }
    
    
    // ACTIONS
    
    // Sign Up button clicked
    
    @IBAction func createAccountAction(_ sender: AnyObject) {
        
        view.endEditing(true)
        ProgressHUD.show("Creating account", interaction: false)
        
        if emailTextField.text == "" {
            
            // FAIL - No email
            
            // Add an alert to the user that the Email field is empty
            
            let alertController = UIAlertController(title: "Error", message: "Please enter your email and password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
            
        } else {
            
            // Use the Firebase method "createUser" to create a new user
            
            Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
                
                //ProgressHUD.showSuccess("Account created")
                
                if error == nil {
                    
                    // SUCCESSFUL - Signed up successfully


                    
                    // Store the users UID
                    currUser.currentUserID = user!.user.uid
                    
                    print(currUser.currentUserID)

                    // Define location to save profile photos
                    let storageRef = Storage.storage().reference(forURL: "gs://seekersnetwork-4465e.appspot.com").child("profileAvatars").child(currUser.currentUserID)
                    if let profileAvatar = self.selectedImageForAvatar, let avatarData = profileAvatar.jpegData(compressionQuality: 0.75){
                        currUser.avatarImage = profileAvatar
                        storageRef.putData(avatarData, metadata: nil, completion: { (metadata, error) in
                            if error != nil {
                                return
                            }
                            storageRef.downloadURL(completion: { (url, error) in
                                if (error == nil) {
                                    if let downloadUrl = url {
                                        let downloadStringURL = downloadUrl.absoluteString
                                        
                                        
                                        print(downloadStringURL)
                                        
                                        let LoginHandlerInstance = LoginHandler()
                                        LoginHandlerInstance.addAccountCreationDataToDatabase(displayName: self.displayNameTextField.text!, email: self.emailTextField.text!, avatarImageURL: downloadStringURL, facebookID: "", completionHandler: self.gotoHomeScreen())
                                     
                                    }
                                } else {
                                    // Do something if error
                                }
                            })
                        })
                    }
                } else {
                    
                    // FAIL - Server side fails (email already exsists...)
                    
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    func gotoHomeScreen(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
        self.present(vc!, animated: true, completion: nil)
    }
    
    
   
    
    
    // END OF CLASS
}


// Extend class to add methods to open camera roll and add photo to "profileAvatarImageView"

extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageForAvatar = image
            profileAvatarImageView.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}
