//
//  LoginLandingViewController.swift
//  Itinery app
//
//  Created by Josh on 16/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit
import FirebaseAuth
import Hero

class LoginLandingViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if Auth.auth().currentUser != nil {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
            self.present(vc!, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "toLoginViewController":
            let dest = segue.destination as! LoginViewController
            dest.hero.modalAnimationType = .selectBy(presenting: .pageOut(direction: .left), dismissing: .pageIn(direction: .right))
            break
        case "toSignUpViewController":
            let dest = segue.destination as! SignUpViewController
            dest.hero.modalAnimationType = .selectBy(presenting: .pageOut(direction: .left), dismissing: .pageIn(direction: .right))
            break
        default:
            break
        }
    }
}
