//
//  LoiginViewController.swift
//  Itinery app
//
//  Created by Josh on 16/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit
import SwiftVideoBackground
import  Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         try? VideoBackground.shared.play(view: view, videoName: "welcome-vid", videoType: "mp4", isMuted: true, darkness: 0.25, willLoopVideo: true, setAudioSessionAmbient: true)
    }
}
