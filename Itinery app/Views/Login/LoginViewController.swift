//
//  LoginViewController.swift
//  FirebaseTutorial
//
//  Created by James Dacombe on 16/11/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    
    // OUTLETS
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBackButton: UIButton!
    @IBOutlet weak var emailLoginButton: appUIButton!
    
    
    // VARIABLES
    
    var selectedImageForAvatar: UIImage?
    var currentUserID = String()
    var pulledFacebookAvatarURL = String()
    
    
    
    // OVERRIDES
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailLoginButton.isEnabled = false
        emailLoginButton.alpha = 0.5
        handleTextField()
        
        
        // Add FB login button to the view
        
        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        loginButton.frame = CGRect(x: 37, y: 510, width: 300, height: 50)
        loginButton.delegate = self
        loginButton.readPermissions = ["email"]
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    // FUNCTIONS
    
    // Update login button active state when user details have been entered
    
    func handleTextField() {
        emailTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        passwordTextField.addTarget(self, action: #selector(LoginViewController.textFieldDidChange), for: UIControl.Event.editingChanged)
        
    }
    
    @objc func textFieldDidChange() {
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                emailLoginButton.isEnabled = false
                return
        }
        emailLoginButton.alpha = 1
        emailLoginButton.isEnabled = true
    }
    
    // FB functions
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Did log out of Facebook")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        loginWithFacebook()
    }
    
    func loginWithFacebook(){
        
        // Show progress
        ProgressHUD.show("Logging in with Facebook", interaction: false)
        
        // Get and store access token
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else { return }
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        
        // Auth with Firebase and FB credentrials
        Auth.auth().signInAndRetrieveData(with: credentials, completion: { (user, error) in
            if error != nil {
                print("Something went wrong with Facebook user: ", error ?? "")
                return
            }
            print("User was sucessfully logged in: ", user ?? "")
            
            // Get Firebase UID and run FB Login
            self.currentUserID = user!.user.uid
            let LoginHandlerInstance = LoginHandler()
            LoginHandlerInstance.fbGraphRequest(accessToken: accessTokenString, currentUserID: self.currentUserID, completionHandler: self.gotoHomeScreen())
        })
    }
    
    
    // ACTIONS
    
    // Back dismiss
    
    @IBAction func loginBackButton(_ sender: Any) {
        dismiss(animated: true)
    }
    
    // Login
    
    @IBAction func loginAction(_ sender: AnyObject) {
        
        view.endEditing(true)
        ProgressHUD.show("Logging you in", interaction: false)
        
        if self.emailTextField.text == "" || self.passwordTextField.text == "" {
            
            // Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        
        } else {
            
            Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
                
                ProgressHUD.showSuccess("Sucessfully logged in")
                
                if error == nil {
                    
                    
                    //Print into the console if successfully logged in
                    print("You have successfully logged in")
                    
                    //Go to the HomeViewController if the login is sucessful
                    self.gotoHomeScreen()
                    
                } else {
                    
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func gotoHomeScreen(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
        self.present(vc!, animated: true, completion: nil)
    }
}
