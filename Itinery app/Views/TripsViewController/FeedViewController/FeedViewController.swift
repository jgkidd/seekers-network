 //
//  FeedsViewController.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit
import Hero
import FBSDKLoginKit
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class FeedViewController: UIViewController {
   
    @IBOutlet weak var followingFeedTableView: UITableView!
    @IBOutlet weak var libraryButton: UIButton!
    @IBOutlet weak var searchBarView: searchBarView!
    @IBOutlet weak var profileButton: UIButton!
    
    
    var lastContentOffset: CGFloat = 0
    var savedToLibrary : Bool {
        get {
            return UserDefaults.standard.bool(forKey: "saved")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "saved")
        }
    }
    
    @IBAction func libraryButtonHit(_ sender: Any) {
        // toggle the likes state
        self.savedToLibrary = !self.libraryButton.isSelected
        // set the likes button accordingly
        self.libraryButton.isSelected = self.savedToLibrary
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
            if let currentCell = sender as? FeedsTableViewCell,
                let vc = segue.destination as? OpenPostViewController,
                let currentCellIndex = followingFeedTableView.indexPath(for: currentCell) {
                vc.selectedIndex = currentCellIndex
            
        } else if segue.identifier == "profileTransition"  {
            let dest = segue.destination as! profileViewController
            dest.hero.modalAnimationType = .selectBy(presenting: .slide(direction: .left), dismissing: .slide(direction: .right))
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // populate the table with FeedData
        followingFeedTableView.dataSource = self
        followingFeedTableView.delegate = self
    
        FeedFunctions.readPost(completion: { [weak self] in
            self?.followingFeedTableView.reloadData()
        })
        
        // initialize the library button to reflect the current state
        self.libraryButton.isSelected = self.savedToLibrary
        
        // nudge table down to start below header
        self.followingFeedTableView.contentInset = UIEdgeInsets(top: 55, left: 0, bottom: 0, right: 0)

        // Dismiss keyboard on drag outside of keyboard
        followingFeedTableView.keyboardDismissMode = .onDrag
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // update profile photo
        profileButton.avatarImageUpdate()
        

        
        
        
    }
    // END OF CLASS
 }
 
 
 
extension FeedViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FeedData.feedModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FeedsTableViewCell
        
        cell.setup(feedModel: FeedData.feedModels[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 508
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(self.lastContentOffset > scrollView.contentOffset.y + 2) &&
            self.lastContentOffset < (scrollView.contentSize.height - scrollView.frame.height) {
            UIView.animate(withDuration: 0.3){
                self.searchBarView.transform = CGAffineTransform(translationX: 0, y: 0)
            }
        }else if (self.lastContentOffset < scrollView.contentOffset.y
            && scrollView.contentOffset.y > 0) {
            UIView.animate(withDuration: 0.3){
                self.searchBarView.transform = CGAffineTransform(translationX: 0, y: self.searchBarView.frame.height)
            }
        }
        self.lastContentOffset = scrollView.contentOffset.y
    }
    
}
