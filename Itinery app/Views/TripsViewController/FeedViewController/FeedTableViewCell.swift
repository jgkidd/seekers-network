//
//  TripsTableViewCell.swift
//  Itinery app
//
//  Created by Josh on 07/11/2018.
//  Copyright © 2018 Josh. All rights reserved.
//

import UIKit

class FeedsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var CreatorsProfileAvatar: UIImageView!
    @IBOutlet weak var reviewCount: UILabel!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var costOfGuide: UILabel!
    @IBOutlet weak var numberOfShares: UILabel!
    @IBOutlet weak var postCommentsController: UIView!
    @IBOutlet weak var viewersProfileAvatar: UIImageView!
    @IBOutlet weak var postCommentTextField: UITextField!
    @IBOutlet weak var numberOfComments: UILabel!
    @IBOutlet weak var saveToLibraryButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    
    
    var isSavedToLibrary : Bool {
        get { return UserDefaults.standard.bool(forKey: "saved")
        }
        set { UserDefaults.standard.set(newValue, forKey: "saved")
        }
    }
    
    var isLiked : Bool {
        get { return UserDefaults.standard.bool(forKey: "liked")
        }
        set { UserDefaults.standard.set(newValue, forKey: "liked")
        }
    }
    
    @IBAction func saveToLibrary(_ sender: Any) {
        self.isSavedToLibrary = !self.saveToLibraryButton.isSelected
        self.saveToLibraryButton.isSelected = self.isSavedToLibrary
    }
    
    
    @IBAction func likeIt(_ sender: Any) {
        self.isLiked = !self.likeButton.isSelected
        self.likeButton.isSelected = self.isLiked
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
         cardView.addShadowAndRoundedCorners()
         titleLabel.font = UIFont(name: Theme.mainFontName, size: 50)
         cardView.backgroundColor = Theme.cards
        //tripImageView.layer.cornerRadius = cardView.layer.cornerRadius
        postCommentsController.layer.cornerRadius = 12
    
    }

    func setup(feedModel: FeedModel) {
        titleLabel.text = feedModel.title
        feedImageView.image = feedModel.guideImage
        CreatorsProfileAvatar.image = feedModel.creatorsAvatar
        reviewCount.text = feedModel.reviewCount
        numberOfLikes.text = feedModel.numberOfLikes
        costOfGuide.text = feedModel.costOfGuide
        numberOfShares.text = feedModel.numberOfShares
        viewersProfileAvatar.image = feedModel.viewersAvatar
        numberOfComments.text = feedModel.numberOfComments
    }
    
    
}
